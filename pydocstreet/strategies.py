import matplotlib.pyplot as plt
from matplotlib.ticker import MaxNLocator

def visualize_strategy_results(results):

    strategies = []
    corrects = []
    incorrects = []
    
    for strategy, (correct, active, total) in results.items():
    
        strategies.append(strategy)
        corrects.append(correct)
        incorrects.append(active-correct)
        
    width = 0.3

    fig, ax = plt.subplots() 

    ax.barh(strategies, corrects, width, 
            label ='Correct') 

    ax.barh(strategies, incorrects, width,  
            left = corrects,  
            label ='Incorrect') 

    ax.set_xlabel('Documents') 
    ax.xaxis.set_major_locator(MaxNLocator(integer=True))
    
    ax.legend()   
    plt.show() 
    
def summarize_strategy_results(docs):

    data_per_field = {}
    
    for doc in docs:
        for fieldname, strategies in doc.dta_output_with_strategies.items():

            if len(strategies) == 0:
                continue

            if fieldname not in data_per_field.keys():
                data_per_field[fieldname] = []

            true_value = '?'

            if fieldname in doc.repair_widget_output:
                true_value = doc.repair_widget_output[fieldname]

            if true_value != '?':
                for strategy_name, strategy_result in strategies.items():                        
                    if strategy_result is None:                            
                        data_per_field[fieldname].append({strategy_name: None})
                    else:
                        data_per_field[fieldname].append({strategy_name: true_value == strategy_result})

    strategy_summary_per_field = {}
                        
    for fieldname, results in data_per_field.items():
        strategy_summary_per_field[fieldname] = {}        
        all_strategies = set([strategy_name for result in results for strategy_name in result.keys()])
        
        for strategy in all_strategies:
            active = sum([1 for result in results if strategy in result and result[strategy] is not None])
            correct = sum([1 for result in results if strategy in result and result[strategy]])
            strategy_summary_per_field[fieldname][strategy] = (correct,active,len(results))

    return strategy_summary_per_field