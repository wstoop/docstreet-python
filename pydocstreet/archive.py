from os.path import isfile
from xml.etree.ElementTree import fromstring
from enum import Enum
from collections import Counter
from re import compile

from boto3 import client as boto_client
from botocore.exceptions import ClientError
from abbyydoc import AbbyyDoc

from find_values_in_doc import *

INDEX_BUCKET = 'sagemaker-training-jobs-822284415333'
BUCKET_NAME = 'docstreet-dms-'
MAPPING_FOLDER = 'skydoo_mappings/'

class Environment(Enum):
    dev = 'dev'
    test = 'test'
    acc = 'acc'
    prod = 'prod'
    
class NoDTAOutputException(Exception):
    pass

class NoRepairWidgetOutputException(Exception):
    pass
    
class Document():

    document_type = None
    last_modified = None

    ocr_output_available = False
    ocr_output = None
    
    dta_output_available = False
    dta_output = {}
    dta_output_with_strategies = {}
    dta_fields_with_strategies = []

    repair_widget_output_available = False
    repair_widget_output = {}
    repair_widget_output_unmapped = {}
    
    customer_code = ''
    label_code = ''
    last_modified = None
    
    def __init__(self,uid,environment):
        self.uid = uid
        self.environment = environment
        
        self.dta_output = {}
        self.dta_output_with_strategies = {}
        self.dta_fields_with_strategies = []
        
        self.repair_widget_output = {}
        self.repair_widget_output_unmapped = {}
      
    def __repr__(self):
        return '<Document '+str(self.uid)+'>'

    def get_corrected_fields(self):
        
        corrected_fields = []
        
        if not (self.dta_output_available) or len(self.dta_output) == 0:
            raise NoDTAOutputException
        
        if not (self.repair_widget_output_available) or len(self.repair_widget_output_unmapped) == 0:
            raise NoRepairWidgetOutputException
            
        for dta_field, dta_value in self.dta_output.items():
            
            if dta_field in self.repair_widget_output_unmapped.keys() and self.repair_widget_output_unmapped[dta_field] != dta_value:
                corrected_fields.append(dta_field)
                
        return corrected_fields
    
def update_index(index_folder='tmp_index/',environment = Environment.prod,continue_after=None):
    WRITE_EVERY_N_FILES = 100    
    nr_of_files_collected = 0

    document_info_per_doctype = {}
    index_per_doctype = {}

    if environment == 'acc':
        environment = Environment.acc
    
    client = boto_client('s3')
    bucket_name = BUCKET_NAME + environment.value
    continuation_token = None
    
    while True:
        print('Collected',nr_of_files_collected,'files')
        kwargs = {'Bucket':bucket_name, 'Prefix':'', 'Delimiter':'/', 'MaxKeys':1000}
        
        if continue_after != None:
            kwargs['StartAfter'] = continue_after
        
        if continuation_token != None:
            kwargs['ContinuationToken'] = continuation_token
        
        objects = client.list_objects_v2(**kwargs)
        
        for o in objects.get('CommonPrefixes'):
            uid = o.get('Prefix')
            document_folder_name = uid+'document/'

            try:
                first_document_path = client.list_objects_v2(Bucket=bucket_name, Prefix=document_folder_name, Delimiter='/')['Contents'][0]['Key']
                data = client.head_object(Bucket=bucket_name, Key=first_document_path)
                document_type = data['Metadata']['documenttype'].replace('/','')
                available_data = [prefix['Prefix'][37:-1] for prefix in client.list_objects_v2(Bucket=bucket_name, Prefix=uid, Delimiter='/')['CommonPrefixes']]
            except KeyError:
                continue
            
            if document_type not in document_info_per_doctype.keys():
                document_info_per_doctype[document_type] = [uid+' '+' '.join(available_data)]
            else:
                document_info_per_doctype[document_type].append(uid+' '+' '.join(available_data))
                
            nr_of_files_collected += 1
            
            if nr_of_files_collected % WRITE_EVERY_N_FILES == 0:
                
                for doctype, info in document_info_per_doctype.items():
                    
                    if doctype == '':
                        continue
                    
                    if doctype not in index_per_doctype.keys():
                        index_per_doctype[doctype] = open(index_folder+doctype,'a')
                    
                    index_per_doctype[doctype].write(''.join([str(i)+'\n' for i in info]))
                    index_per_doctype[doctype].flush()
                
                document_info_per_doctype = {}
              
        if objects.get('IsTruncated'):
            continuation_token = objects.get('NextContinuationToken')
        else:
            break

def add_repair_widget_subfields_to_dict(d,root,query,namespace,prefix=''):

    for field_type in ['Text','Date','Integer','Decimal']:
        for field in root.findall(query+field_type+'Field',namespace):
            field_name = field.attrib['name']

            try:
                value = field.findall('asys:Value',namespace)[0].text
            except IndexError:
                value = ''

            d[prefix+field_name] = value            

def process_mapping_child(child,mapping,mapping_reverse,original_prefix,mapped_prefix, iteration = None):

    original = child.attrib['mapping']
    mapped = child.attrib['name']

    if 'FieldGroupDef' in child.tag:
        for grandchild in child:

            if 'repeatable' in grandchild.attrib and grandchild.attrib['repeatable'] == 'true':

                for i in range(9):
                    process_mapping_child(grandchild, mapping, mapping_reverse, original_prefix + '/' + original,
                                      mapped,i+1)
            else:

                if iteration == None:
                    process_mapping_child(grandchild, mapping, mapping_reverse, original_prefix + '/' + original,
                                      mapped)
                else:
                    process_mapping_child(grandchild, mapping, mapping_reverse, original_prefix + '/' + original + '-' + str(iteration),
                                      mapped+'-'+str(iteration))

    else:

        if original_prefix != '':
            original_prefix = '/'+original_prefix+'/'

        if mapped_prefix != '':
            mapped_prefix = mapped_prefix+'-'

        mapping[original_prefix+original] = mapped
        mapping_reverse[mapped_prefix+mapped] = original_prefix+original

def mapping_file_to_mapping(mapping_file_location):
    
    mapping = {}
    mapping_reverse = {}

    for field in fromstring(open(mapping_file_location).read()):
        process_mapping_child(field,mapping,mapping_reverse,'','')
    
    return mapping, mapping_reverse