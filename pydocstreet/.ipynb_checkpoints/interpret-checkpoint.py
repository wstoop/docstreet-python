from xml.etree.ElementTree import fromstring

from boto3 import client as boto_client
from botocore.exceptions import ClientError

def get_dta_output(bucket_name,filepath, include_strategies=False):

    client = boto_client('s3')   
    output = {}
    output_strategies = {}
    
    try:
        dta_output = client.get_object(Bucket=bucket_name, Key=filepath)['Body'].read().decode()
        parsed_xml = fromstring(dta_output)

        NAMESPACE = {'dta': 'http://www.davincigroep.nl/2013/11/dta'}               

        for field in parsed_xml.findall('dta:Analysis/dta:Result/dta:FieldGroup/dta:Field', NAMESPACE):
            field_name = field.attrib['name']

            strategies = {}
            for strategy in field.findall('dta:Strategies/dta:Strategy', NAMESPACE):
                try:
                    strategies[strategy.attrib['name']] = strategy.findall('dta:Value', NAMESPACE)[0].text
                except IndexError:
                    strategies[strategy.attrib['name']] = None
            output_strategies[field_name] = strategies
            
            try:
                value = field.findall('dta:Value', NAMESPACE)[0].text
                output[field_name] = value
            except IndexError:
                continue
                
        for field in parsed_xml.findall('dta:Analysis/dta:Result/dta:FieldGroup/dta:FieldGroup/dta:FieldGroup', NAMESPACE):

            parent_name = field.attrib['name']

            for subfield in field.findall('dta:Field',NAMESPACE):
                field_name = subfield.attrib['name']
                value = subfield.findall('dta:Value', NAMESPACE)[0].text
                output[parent_name+'-'+field_name] = value

        for field in parsed_xml.findall('dta:Analysis/dta:Result/dta:FieldGroup/dta:FieldGroup', NAMESPACE):

            parent_name = field.attrib['name']

            for subfield in field.findall('dta:Field',NAMESPACE):
                field_name = subfield.attrib['name']
                value = subfield.findall('dta:Value', NAMESPACE)[0].text
                output[parent_name+'-'+field_name] = value                    

        if include_strategies:
            return output, output_strategies
        else:
            return output
    
    except ClientError:
        return None 
    
def add_repair_widget_subfields_to_dict(d,root,query,namespace,prefix=''):
    
    for field_type in ['Text','Date','Integer','Decimal']:
        for field in root.findall(query+field_type+'Field',namespace):
            field_name = field.attrib['name']
            
            try:
                value = field.findall('asys:Value',namespace)[0].text
            except IndexError:
                value = ''

            d[prefix+field_name] = value          
    
def get_repair_widget_output(bucket_name, analysis_folder):

    client = boto_client('s3')   
    result = {}
    
    try:
        first_repair_widget_output_path = client.list_objects_v2(Bucket=bucket_name, Prefix=analysis_folder, Delimiter='/')['Contents'][0]['Key']
    except KeyError:
        return None

    try:
        repair_widget_output = client.get_object(Bucket=bucket_name, Key=first_repair_widget_output_path)['Body'].read().decode()
        NAMESPACE = {'dta': 'http://www.davincigroep.nl/2013/11/dta','asys': 'http://www.davincigroep.nl/2014/02/docstreet/analysis'}

        parsed_xml = fromstring(repair_widget_output)
        
        add_repair_widget_subfields_to_dict(result,parsed_xml,'asys:Analysis/asys:Result/asys:FieldGroup/asys:',NAMESPACE)
        #for fieldgroup in parsed_xml.findall('asys:Analysis/asys:Result/asys:FieldGroup/',NAMESPACE):
        #    add_repair_widget_subfields_to_dict(result,parsed_xml,'asys:Analysis/asys:Result/asys:FieldGroup/asys:FieldGroup/asys:',
        #                          NAMESPACE,prefix=fieldgroup.attrib['name']+'-')
        
    except ClientError:
        return None     
    
    return result
    
def process_mapping_child(child,mapping,mapping_reverse,original_prefix,mapped_prefix, iteration = None):

    original = child.attrib['mapping']
    mapped = child.attrib['name']

    if 'FieldGroupDef' in child.tag:
        for grandchild in child:

            if 'repeatable' in grandchild.attrib and grandchild.attrib['repeatable'] == 'true':

                for i in range(9):
                    process_mapping_child(grandchild, mapping, mapping_reverse, original_prefix + '/' + original,
                                      mapped,i+1)
            else:

                if iteration == None:
                    process_mapping_child(grandchild, mapping, mapping_reverse, original_prefix + '/' + original,
                                      mapped)
                else:
                    process_mapping_child(grandchild, mapping, mapping_reverse, original_prefix + '/' + original + '-' + str(iteration),
                                      mapped+'-'+str(iteration))

    else:

        if original_prefix != '':
            original_prefix = '/'+original_prefix+'/'

        if mapped_prefix != '':
            mapped_prefix = mapped_prefix+'-'

        mapping[original_prefix+original] = mapped
        mapping_reverse[mapped_prefix+mapped] = original_prefix+original

def mapping_file_to_mapping(mapping_file_location):
    
    mapping = {}
    mapping_reverse = {}

    for field in fromstring(open(mapping_file_location).read()):
        process_mapping_child(field,mapping,mapping_reverse,'','')
    
    return mapping, mapping_reverse