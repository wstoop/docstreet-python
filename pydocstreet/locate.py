import datetime

from enum import Enum
from collections import Counter
from re import compile

class FieldType(Enum):
    date = 'date'
    percentage = 'percentage'
    money = 'money'

def find_values_in_abbyy_output(abbyydoc,value_dict, field_types = None, deprecated_word_finding=False):
        
    matches = {}
        
    for field, value in value_dict.items():
        
        matches[field] = []
    
        if value in [None,'']:
            continue
            
        if field_types != None and field in field_types.keys():
            if FieldType.date in field_types[field]:
                value = string_to_uniform_date(value)
            elif FieldType.percentage in field_types[field]:
                value = string_to_uniform_percentage(value)
            elif FieldType.money in field_types[field]:
                value = string_to_uniform_money(value)
            
        for page in abbyydoc.pages:
            for block in page.blocks:

                is_match = False

                if not deprecated_word_finding:
                    for word in block.calculate_words(add_extra_spaces=True):

                        is_match = False
                        
                        if field_types == None:
                            if value == word.content:
                                is_match = True

                        elif field in field_types.keys() and FieldType.date in field_types[field] and value == string_to_uniform_date(word.content):
                            is_match = True

                        elif field in field_types.keys() and FieldType.percentage in field_types[field] and value == string_to_uniform_percentage(word.content):
                            is_match = True

                        elif field in field_types.keys() and FieldType.money in field_types[field] and value == string_to_uniform_money(word.content):
                            is_match = True                        

                        elif value.strip().lower() == word.content.strip().lower():
                            is_match = True

                        if is_match:
                            matches[field].append(word)   
                    
                else:
                    for line in block.get_lines() + block.get_lines(add_extra_spaces=True):
                        for word in line.split() + [line]:
                            
                            if field_types == None:
                                if value == word:
                                    is_match = True
                                    break

                            elif field in field_types.keys() and FieldType.date in field_types[field] and value == string_to_uniform_date(word):
                                is_match = True
                                break

                            elif field in field_types.keys() and FieldType.percentage in field_types[field] and value == string_to_uniform_percentage(word):
                                is_match = True
                                break

                            elif field in field_types.keys() and FieldType.money in field_types[field] and value == string_to_uniform_money(word):
                                is_match = True                        
                                break

                            elif value.strip().lower() == word.strip().lower():
                                is_match = True
                            break
                        
                        if is_match:
                            break                            

                    if is_match:
                        matches[field].append(block)                        

    return matches

def detect_field_types(docs):
    
    LOW_BOUNDARY = 0.01
    HIGH_BOUNDARY = 0.75
    
    strict_regex_per_field_type = {FieldType.percentage: compile('\d{2}'),
                            FieldType.money: compile('\d{1,4}\.\d{2}'),
                            FieldType.date: compile('\d{4}-\d{2}-\d{2}[T\d{2}:\d{2}:\d{2}\+\d{2}:\d{2}]?')}

    loose_regex_per_field_type = {FieldType.percentage: compile('1?\d{1,2}(\.\d{1,4})?'),
                            FieldType.money: compile('\d{1,12}(\.\d{1,2})?'),
                            FieldType.date: compile('\d{4}-\d{2}-\d{2}[T\d{2}:\d{2}:\d{2}\+\d{2}:\d{2}]?')}

    total_per_field = Counter()
    loose_match_per_field = {}
    strict_match_per_field = {}

    options_per_field = {}
    examples_per_field = {}

    #Go through all values and collect what are the options
    for doc in docs:
        
        data = doc.dta_output
        
        if len(data) == 0:
            data = doc.repair_widget_output_unmapped
        
        for field, value in data.items():

            if value in [None,'',' ']:
                continue

            total_per_field[field] += 1

            if not field in loose_match_per_field.keys():
                loose_match_per_field[field] = {field_type: 0 for field_type in FieldType}
                strict_match_per_field[field] = {field_type: 0 for field_type in FieldType}

            for field_type in FieldType:

                if loose_regex_per_field_type[field_type].fullmatch(value):
                    loose_match_per_field[field][field_type] += 1

                    if strict_regex_per_field_type[field_type].fullmatch(value):
                        strict_match_per_field[field][field_type] += 1

            try:
                examples_per_field[field].append(value)
            except KeyError:
                examples_per_field[field] = [value]

    for field,total in total_per_field.items():

        options_per_field[field] = []

        for field_type in FieldType:
            print(field,field_type,loose_match_per_field[field][field_type]/total,strict_match_per_field[field][field_type]/total,loose_match_per_field[field][field_type]/total > HIGH_BOUNDARY,strict_match_per_field[field][field_type]/total > LOW_BOUNDARY)
            if loose_match_per_field[field][field_type]/total > HIGH_BOUNDARY and strict_match_per_field[field][field_type]/total > LOW_BOUNDARY:
                options_per_field[field].append(field_type)
                
    return options_per_field

def string_to_uniform_date(s):

    if len(s) > 10:
        try:
            datetime_obj = datetime.datetime.strptime(s[:19],'%Y-%m-%dT%H:%M:%S')
            return '{:02d}/'.format(datetime_obj.day) + '{:02d}/'.format(datetime_obj.month) + '{:04d}'.format(datetime_obj.year)
        
        except ValueError:    
            return ''
    
    s = s.replace('-', '/')

    parts = s.split('/')

    #Swap if needed
    try:
        if len(parts[0]) == 4:
            parts.reverse()        
    except IndexError:
        pass
    
    try:        
        if len(parts[0]) == 1:
            parts[0] = '0' + parts[0]

        if len(parts[1]) == 1:
            parts[1] = '0' + parts[1]

        if len(parts[2]) == 2:
            parts[2] = '19' + parts[2]

        s = '/'.join(parts)
    except IndexError:
        pass

    if len(s) == 8 and ('19' in s or '20' in s):
        s = s[0] + s[1] + '/' + s[2] + s[3] + '/' + s[4] + s[5] + s[6] + s[7]
    
    return s

def string_to_uniform_money(s):
    s = s.replace(',','.').replace('-','')

    if s.count('.') > 1:
        parts = s.split('.')
        s = ''.join(parts[:-1]) + '.' + parts[-1]

    if len(s) > 1 and s[-1] == '0':
        s = s[:-1]

    return s

def string_to_uniform_percentage(s):

    s = s.replace(',', '.').replace('-','')

    if len(s) > 1 and s[-1] == '0':
        s = s[:-1]

    return s