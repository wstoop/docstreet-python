from os import mkdir, listdir
from os.path import isdir
from difflib import ndiff
from copy import deepcopy
from numpy import zeros
from collections import Counter
from json import dump

from Levenshtein import distance

from abbyydoc import AbbyyDoc

class Cell():

    def __init__(self,top,bottom,left,right):
        self.top = int(top)
        self.bottom = int(bottom)
        self.left = int(left)
        self.right = int(right)

class LowResDocMatrixGenerator():

    RESOLUTION = (30,40)
    MINIMUM_CELL_OCCUPATION = 0.15
    MINIMUM_FREQ = 2
    LEVENSHTEIN_THRESHOLD = 3

    def __init__(self):
        self.word_to_index = {}
        self.index_to_word = {}
        self.vocabulary = Counter()

    def overlap(self,cell, entity):
        
        if entity.right == None or entity.left == None or entity.bottom == None or entity.top == None:
            return False
        
        dx = min(cell.right, entity.right) - max(cell.left, entity.left)
        dy = min(cell.bottom, entity.bottom) - max(cell.top, entity.top)

        if (dx >= 0) and (dy >= 0):
            return dx * dy
        else:
            return False
        
    def fit(self,docs):

        documents = []
        cellsizes = []

        for doc in docs:

            entities = []

            for block in doc.pages[0].blocks:

                for word in block.calculate_words(add_extra_spaces=True):      
                    
                    if word.right == None:
                        word.right = 0
                        
                    if word.bottom == None:
                        word.bottom = 0                    
                    
                    entities.append(word)
                    self.vocabulary[word.content] += 1

            if len(entities) == 0:
                continue

            docsize = (max([entity.right for entity in entities]), max([entity.bottom for entity in entities]))
            cellsizes.append((round(docsize[0] / self.RESOLUTION[0]), round(docsize[1] / self.RESOLUTION[1])))

            documents.append(entities)

        # Build the word index
        current_index = 1

        for word, freq in self.vocabulary.most_common():

            if freq < self.MINIMUM_FREQ:

                smallest_levenshtein_distance = 999
                best_match = None

                for possible_correct,_ in self.vocabulary.most_common(100):
                    levenshtein_distance = distance(word,possible_correct)
                    if levenshtein_distance < self.LEVENSHTEIN_THRESHOLD and levenshtein_distance < smallest_levenshtein_distance:
                        smallest_levenshtein_distance = levenshtein_distance
                        best_match = possible_correct

                if best_match != None:
                    word = best_match
                else:
                    word = str_to_pattern(word)

            if word not in self.word_to_index.keys():
                self.word_to_index[word] = current_index
                self.index_to_word[current_index] = word

                current_index += 1

    def calculate_matrices(self,docs,verbose=False):

        matrices = []

        #Now we have the full vocab, go over all files again        
        for n, doc in enumerate(docs):
            
            print('* Building matrix',n,'/',len(docs))
            entities = []
            for block in doc.pages[0].blocks:

                for word in block.calculate_words(add_extra_spaces=True):
                    entities.append(word)
                                        
                    if word.right == None:
                        word.right = 0
                        
                    if word.bottom == None:
                        word.bottom = 0

                    self.vocabulary[word.content] += 1

            if len(entities) == 0:
                continue

            #Prepare vars
            lowresdocmatrix = zeros((self.RESOLUTION[1],self.RESOLUTION[0]),dtype=int)
            used_entities = set()

            #Iterate over all cells to see what's in it
            docsize = (max([entity.right for entity in entities]), max([entity.bottom for entity in entities]))
            cellsize = (round(docsize[0] / self.RESOLUTION[0]), round(docsize[1] / self.RESOLUTION[1]))

            for x in range(self.RESOLUTION[0]):
                for y in range(self.RESOLUTION[1]):
                    cell = Cell(y*cellsize[1],(1+y)*cellsize[1],x*cellsize[0],(1+x)*cellsize[0])

                    contender = None
                    highest_overlap = 0

                    for entity in entities:
                        overlap_percentage = self.overlap(cell,entity)

                        if overlap_percentage and overlap_percentage > self.MINIMUM_CELL_OCCUPATION and overlap_percentage > highest_overlap:
                            contender = entity
                            highest_overlap = overlap_percentage

                    if contender != None:
                        content = contender.content

                        try:
                            content = self.word_to_index[content]
                        except KeyError:
                            try:
                                content = self.word_to_index[str_to_pattern(content)]
                            except KeyError:
                                continue

                        lowresdocmatrix[y,x] = content
                        used_entities.add(contender)

            matrices.append(LowResDocMatrix(lowresdocmatrix,cellsize))

        return matrices
    
class LowResDocMatrix():
    
    def __init__(self,matrix,cellsize):
        
        self.cellsize = cellsize
        self.matrix = matrix
    
    def get_cells_overlapping_with_area(self,left,right,top,bottom):

        cells = []
        overlap = []
        
        for x in range(self.matrix.shape[1]):
            for y in range(self.matrix.shape[0]):
                cell_top = y*self.cellsize[1]
                cell_bottom = (1+y)*self.cellsize[1]
                cell_left = x*self.cellsize[0]
                cell_right = (1+x)*self.cellsize[0]
                
                dx = min(cell_right, right) - max(cell_left, left)
                dy = min(cell_bottom, bottom) - max(cell_top, top)

                if (dx >= 0) and (dy >= 0):
                    cells.append((x,y))
                    overlap.append((dx*dy)/((bottom-top)*(right-left)))
                    
        return cells,overlap
    
def get_content_in_matrix_area(matrix,index_to_word_dictionary,left,right,top,bottom):
    
    cells, overlap = matrix.get_cells_overlapping_with_area(left,right,top,bottom)                
    results = []
    
    for cell, overlap in zip(cells,overlap):

        try:
            results.append((index_to_word_dictionary[matrix.matrix[(cell[1],cell[0])]],overlap))
        except KeyError:
            results.append(('UNKNOWN_WORD',overlap))
            
    return results

def get_middle_cell_in_area(matrix,left,right,top,bottom):

    cells, overlap = matrix.get_cells_overlapping_with_area(left,right,top,bottom)                
    
    x_values = [cell[0] for cell in cells]
    x_values.sort()
    middle_x_index = round(len(x_values)/2)
    
    y_values = [cell[1] for cell in cells]
    y_values.sort()
    middle_y_index = round(len(y_values)/2)
    
    return [x_values[middle_x_index], y_values[middle_y_index]]

def get_cell_coordinates_for_match(matrix,match,word_to_index_dictionary):

    try:
        target_content_index = word_to_index_dictionary[match.content]
    except KeyError:
        target_content_index = word_to_index_dictionary[str_to_pattern(match.content)]
    
    result = []
    
    for (y,x) in matrix.get_cells_overlapping_with_area(match.left,match.right,match.top,match.bottom)[0]:
        
        #Check: does this cell have the right content too?
        if matrix.matrix[x,y] == target_content_index:
            result.append((x,y))
        
    return result

def str_to_pattern(s):

    result = ''

    for char in s:

        if char in 'ABCDEFGHIJKLNMNOPQRSTUVWXYZ':
            result += 'a'
        elif char in 'abcdefghijklmnopqrstuvwxyzïöüé¼eëèó':
            result += 'a'
        elif char in '0123456789':
            result += '9'
        elif char in '!@#$%^&*()-=_+€[]{},./<>?:;\'\"|~° ':
            result += '.'
        else:
            result += '?'

    return result