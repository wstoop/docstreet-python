class NoDTAOutputException(Exception):
    pass

class NoRepairWidgetOutputException(Exception):
    pass

class Document():

    document_type = None
    last_modified = None

    ocr_output_available = False
    ocr_output = None
    
    dta_output_available = False
    dta_output = {}
    dta_output_with_strategies = {}
    dta_fields_with_strategies = []

    repair_widget_output_available = False
    repair_widget_output = {}
    repair_widget_output_unmapped = {}
    
    customer_code = ''
    label_code = ''
    last_modified = None
    
    def __init__(self,uid,environment):
        self.uid = uid
        self.environment = environment
        
        self.dta_output = {}
        self.dta_output_with_strategies = {}
        self.dta_fields_with_strategies = []
        
        self.repair_widget_output = {}
        self.repair_widget_output_unmapped = {}
      
    def __repr__(self):
        return '<Document '+str(self.uid)+'>'

    def get_corrected_fields(self):
        
        corrected_fields = []
        
        if not (self.dta_output_available) or len(self.dta_output) == 0:
            raise NoDTAOutputException
        
        if not (self.repair_widget_output_available) or len(self.repair_widget_output_unmapped) == 0:
            raise NoRepairWidgetOutputException
            
        for dta_field, dta_value in self.dta_output.items():
            
            if dta_field in self.repair_widget_output_unmapped.keys() and self.repair_widget_output_unmapped[dta_field] != dta_value:
                corrected_fields.append(dta_field)
                
        return corrected_fields