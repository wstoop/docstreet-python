import xml.etree.ElementTree as ET
from statistics import median, StatisticsError

class AbbyyDoc():

    def __init__(self, filepath):

        # Create basic vars
        self.pages = [];

        # Load in the file
        self._load(filepath);

    def __repr__(self):
        return '<Abbyy result ' + str(len(self.pages)) + ' pages>'

    def _load(self, filepath):

        xmlroot = ET.fromstring(filepath);
        pagenr = 0;

        for page in xmlroot:

            height = page.get('height');
            width = page.get('width');

            if height == None or width == None:
                continue;

            current_page = Page(pagenr, height, width);
            pagenr += 1;

            for block in page:

                id = block.get('pageElemId');
                type = block.get('blockType')
                left = block.get('l');
                right = block.get('r');
                top = block.get('t');
                bottom = block.get('b');

                current_block = Block(id, type, top, left, right, bottom);
                for char in self._block_to_chars(block):
                    content = char.text;
                    left = char.get('l');
                    right = char.get('r');
                    top = char.get('t');
                    bottom = char.get('b');
                    stroke_width = char.get('meanStrokeWidth');
                    confidence = char.get('charConfidence');

                    current_char = Character(content, top, left, right, bottom, stroke_width, confidence);
                    current_block.chars.append(current_char);

                current_page.blocks.append(current_block);

            self.pages.append(current_page);

    def _block_to_chars(self, block):
        """Helper function to get all chars out of a block""";

        chars = [];
        for blockchild in block:
            if blockchild.tag[64:] == 'text':
                for par in blockchild:
                    for line in par:
                        for formatting in line:
                            for char in formatting:
                                chars.append(char);

            elif blockchild.tag[64:] == 'row':
                for cell in blockchild:
                    for text in cell:
                        for par in text:
                            for line in par:
                                for formatting in line:
                                    for char in formatting:
                                        chars.append(char);

        return chars;

    def all_characters(self):

        all_chars = [];

        for page in self.pages[:1]:
            for block in page.blocks:
                all_chars += block.chars;

        return all_chars;

    def get_content(self):

        content = []

        for page in self.pages:
            for block in page.blocks:
                for word in block.calculate_words(add_extra_spaces=True):
                    content.append(word.content)

        return ' '.join(content)


class Page():

    def __init__(self, nr, height, width):
        self.nr = nr;
        self.height = height;
        self.width = width;
        self.blocks = [];


class Block():

    def __init__(self, id, type, top, left, right, bottom):

        self.id = id;
        self.type = type;
        self.top = int(top);
        self.bottom = int(bottom);
        self.left = int(left);
        self.right = int(right);

        self.pos = (self.top, self.left);
        self.size = (self.right - self.left, self.bottom - self.top);

        self.chars = [];

    def __repr__(self):
        return '<Abbyy block ' + self.get_content() + '>'

    def get_median_char_width(self):

        try:
            return median([char.right - char.left for char in self.chars])
        except StatisticsError:
            return 0

    def get_content(self):
        return ''.join([char.content for char in self.chars])

    def get_words(self):
        return self.get_content().split()

    def get_lines(self, add_extra_spaces=False, horizontal_threshold=0.6, vertical_threshold=1000):

        median_char_width = self.get_median_char_width()

        previous_top = 0
        previous_bottom = 0
        previous_right = 0

        line = ''
        lines = []

        for char in self.chars:

            if (char.top - previous_top) ** 2 + (char.bottom - previous_bottom) ** 2 > vertical_threshold:
                lines.append(line)
                line = ''
            elif add_extra_spaces and char.left - previous_right > horizontal_threshold * median_char_width:
                line += ' '

            line += char.content

            previous_top = char.top
            previous_bottom = char.bottom
            previous_right = char.right

        return lines

    def calculate_words(self, add_extra_spaces=False, horizontal_threshold=0.6):

        median_char_width = self.get_median_char_width()

        previous_top = 0
        previous_bottom = 0
        previous_right = 0

        current_word = Word()
        words = []
        VERTICAL_THRESHOLD = 1000

        for char in self.chars:

            if char.content == ' ' or (char.top - previous_top) ** 2 + (char.bottom - previous_bottom) ** 2 > VERTICAL_THRESHOLD or (
                    add_extra_spaces and char.left - previous_right > horizontal_threshold * median_char_width):

                if len(current_word.chars) > 0:
                    words.append(current_word.finalize())

                current_word = Word()

            if char.content not in ['',' ']:
                current_word.add_char(char)
            
            previous_top = char.top
            previous_bottom = char.bottom
            previous_right = char.right

        words.append(current_word.finalize())

        return words

    def debug_positions(self):

        print(self.get_content())
        for char in self.chars:
            print(char.left, char.right)


class Word():
    """Handmade object"""

    def __init__(self):
        self.chars = []
        self.top = None
        self.bottom = None
        self.left = None
        self.right = None
        self.content = ''

    def add_char(self, char):
        self.chars.append(char)

        if self.top == None or char.top < self.top:
            self.top = char.top

        if self.bottom == None or char.bottom > self.bottom:
            self.bottom = char.bottom

        if self.left == None or char.left < self.left:
            self.left = char.left

        if self.right == None or char.right > self.right:
            self.right = char.right

        self.content += char.content

    def finalize(self):

        if len(self.chars) > 0:
            self.width = self.right - self.left
            self.height = self.bottom - self.top
        else:
            self.width = 0
            self.height = 0

        self.pos = (self.top, self.left)
        self.size = (self.width, self.height)

        return self


class Character():

    def __init__(self, content, top, left, right, bottom, stroke_width, confidence):

        if content == None:
            self.content = ''
        else:
            self.content = content;

        self.top = int(top);
        self.bottom = int(bottom);
        self.left = int(left);
        self.right = int(right);

        self.pos = (self.top, self.left);
        self.width = self.right - self.left;
        self.height = self.bottom - self.top;
        self.size = (self.width, self.height);

        try:
            self.stroke_width = int(stroke_width);
        except TypeError:  # Sometimes there is not stroke width (spaces for example)
            self.stroke_width = 0;

        try:
            self.confidence = int(confidence);
        except TypeError:
            self.confidence = 0;