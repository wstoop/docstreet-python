from enum import Enum

from boto3 import client as boto_client
from botocore.exceptions import ClientError

from pydocstreet.document import Document
from pydocstreet.abbyydoc import AbbyyDoc
from pydocstreet.interpret import get_dta_output, get_repair_widget_output

INDEX_BUCKET = 'sagemaker-training-jobs-822284415333'
BUCKET_NAME = 'docstreet-dms-'
MAPPING_FOLDER = 'skydoo_mappings/'

class Environment(Enum):
    dev = 'dev'
    test = 'test'
    acc = 'acc'
    prod = 'prod'
    
def update_index(index_folder='tmp_index/',environment = Environment.prod,continue_after=None):
    WRITE_EVERY_N_FILES = 100    
    nr_of_files_collected = 0

    document_info_per_doctype = {}
    index_per_doctype = {}

    if environment == 'acc':
        environment = Environment.acc
    
    client = boto_client('s3')
    bucket_name = BUCKET_NAME + environment.value
    continuation_token = None
    
    while True:
        print('Collected',nr_of_files_collected,'files')
        kwargs = {'Bucket':bucket_name, 'Prefix':'', 'Delimiter':'/', 'MaxKeys':1000}
        
        if continue_after != None:
            kwargs['StartAfter'] = continue_after
        
        if continuation_token != None:
            kwargs['ContinuationToken'] = continuation_token
        
        objects = client.list_objects_v2(**kwargs)
        
        for o in objects.get('CommonPrefixes'):
            uid = o.get('Prefix')
            document_folder_name = uid+'document/'

            try:
                first_document_path = client.list_objects_v2(Bucket=bucket_name, Prefix=document_folder_name, Delimiter='/')['Contents'][0]['Key']
                data = client.head_object(Bucket=bucket_name, Key=first_document_path)
                document_type = data['Metadata']['documenttype'].replace('/','')
                available_data = [prefix['Prefix'][37:-1] for prefix in client.list_objects_v2(Bucket=bucket_name, Prefix=uid, Delimiter='/')['CommonPrefixes']]
            except KeyError:
                continue
            
            if document_type not in document_info_per_doctype.keys():
                document_info_per_doctype[document_type] = [uid+' '+' '.join(available_data)]
            else:
                document_info_per_doctype[document_type].append(uid+' '+' '.join(available_data))
                
            nr_of_files_collected += 1
            
            if nr_of_files_collected % WRITE_EVERY_N_FILES == 0:
                
                for doctype, info in document_info_per_doctype.items():
                    
                    if doctype == '':
                        continue
                    
                    if doctype not in index_per_doctype.keys():
                        index_per_doctype[doctype] = open(index_folder+doctype,'a')
                    
                    index_per_doctype[doctype].write(''.join([str(i)+'\n' for i in info]))
                    index_per_doctype[doctype].flush()
                
                document_info_per_doctype = {}
              
        if objects.get('IsTruncated'):
            continuation_token = objects.get('NextContinuationToken')
        else:
            break
    
def collect_documents(**kwargs):
        
    environment = Environment.prod
    document_type = 'SalarySlip'
    max_docs = 100
    index_folder = 'index_v2/'

    ocr_output_should_be_available = False
    dta_output_should_be_available = False
    repair_widget_output_should_be_available = False
    
    load_ocr_output = False
    load_dta_output = False
    load_repair_widget_output = False
    
    for key, value in kwargs.items():            
        
        if key == 'environment':
            if value == 'acc':
                environment = Environment.acc
        elif key == 'document_type':
            document_type = value
        elif key == 'max_docs':
            max_docs = value
        elif key == 'index_folder':
            index_folder = value
            if value[-1] not in '/\\':
                index_folder =+ '/'
 
        elif key == 'ocr_output_available':
            ocr_output_should_be_available = value
        elif key == 'dta_output_available':
            dta_output_should_be_available = value
        elif key == 'repair_widget_output_available':
            repair_widget_output_should_be_available = value
            
        elif key == 'load_ocr_output':
            load_ocr_output = value
        elif key == 'load_dta_output':
            load_dta_output = value
        elif key == 'load_repair_widget_output':
            load_repair_widget_output = value

    info_per_document_id = {}
    for document_info in open(index_folder+document_type):
        document_info = document_info.strip().split()
        info_per_document_id[document_info[0]] = document_info[1:]
      
    bucket_name = BUCKET_NAME+environment.value           
    result = []       
    client = boto_client('s3')   

    for document_id, document_info in info_per_document_id.items():
        
        if ocr_output_should_be_available and 'ocr' not in document_info:
            continue
            
        if dta_output_should_be_available and 'dta' not in document_info:
            continue
            
        if repair_widget_output_should_be_available and 'analysis' not in document_info:
            continue
        
        document = Document(document_id,environment)
        document.document_type = document_type
        document.ocr_output_available = 'ocr' in document_info
        document.dta_output_available = 'dta' in document_info
        document.repair_widget_output_available = 'analysis' in document_info

        document_okay = True        
        document_folder_name = document_id+'document/'
        try:
            first_document_path = client.list_objects_v2(Bucket=bucket_name, Prefix=document_folder_name, Delimiter='/')['Contents'][0]['Key']      
            response = client.head_object(Bucket=bucket_name, Key=first_document_path)
            metadata = response['Metadata']
        
            try:
                document.customer_code = metadata['customercode']
            except KeyError:
                pass

            try:
                document.label_code = metadata['labelcode']
            except KeyError:
                pass
            
            try:
                document.last_modified = response['LastModified']
            except KeyError:
                pass
            
        except KeyError:
            pass
            
        # ======== Add ocr output ==========
        if load_ocr_output and document.ocr_output_available:
            ocr_folder = document.uid + 'ocr/'
            
            try:
                first_ocr_output_path = client.list_objects_v2(Bucket=bucket_name, Prefix=ocr_folder, Delimiter='/')['Contents'][0]['Key']
                ocr_output = client.get_object(Bucket=bucket_name, Key=first_ocr_output_path)['Body'].read().decode()
                document.ocr_output = AbbyyDoc(ocr_output)

            except KeyError:
                pass
                
        # ======= Add dta output ===========
        if load_dta_output and document.dta_output_available:
            dta_folder = document.uid + 'dta/'

            try:
                first_dta_output_path = client.list_objects_v2(Bucket=bucket_name, Prefix=dta_folder, Delimiter='/')['Contents'][0]['Key']
                document.dta_output = get_dta_output(bucket_name,first_dta_output_path)                         
            except KeyError:
                pass
            
            if len(document.dta_output) == 0:
                document_okay = False

        # ======= Add repair widget output ============
        if load_repair_widget_output and document.repair_widget_output_available:           
            analysis_folder = document.uid + 'analysis/'
            document.repair_widget_output = get_repair_widget_output(bucket_name,analysis_folder)                                   

        # ===== See if we can created an unmapped (= non translated) version of the repair widget output
        mapping_file = MAPPING_FOLDER+document.document_type+'.xml'
        
        if load_repair_widget_output and document.repair_widget_output_available and isfile(mapping_file):                      
            mapping, mapping_reverse = mapping_file_to_mapping(mapping_file)
            
            fields_that_could_not_be_mapped = [] #unused
            
            for field,value in document.repair_widget_output.items():
                if field in mapping_reverse.keys():
                    parts = mapping_reverse[field].split('/')
                    
                    if len(parts) > 4:
                        key = '-'.join(parts[-2:])
                    else:
                        key = parts[-1]
                    
                    document.repair_widget_output_unmapped[key] = value
                else:
                    fields_that_could_not_be_mapped.append(field)
                                              
        #try:
            #first_document_path = client.list_objects_v2(Bucket=bucket_name, Prefix=document_folder_name, Delimiter='/')['Contents'][0]['Key']
            #data = client.head_object(Bucket=bucket_name, Key=first_document_path)
            #document.last_modified = data['LastModified']
            
        #except KeyError:
        #    pass    
    
        if document_okay:
            result.append(document)

        if len(result) >= max_docs:
            break

    return result    
    
def collect_recent_dta_and_repair_widget_output(month_nr,doctype,customercode,environment, include_strategies=True):
    
    bucket = 'docstreet-dms-'+environment
    client = boto_client('s3')
    continuation_token = None
    
    i = 0
    while True:

        if continuation_token == None:
            response = client.list_objects_v2(Bucket=bucket)
        else:
            response = client.list_objects_v2(Bucket=bucket,ContinuationToken=continuation_token)

        continuation_token = response['NextContinuationToken']

        for f in response['Contents']:

            if f['LastModified'].month != month_nr or '/dta/' not in f['Key']:                       
                continue            
            
            metadata = client.head_object(Bucket=bucket, Key=f['Key'])

            if metadata['Metadata']['document-type'] != doctype:
                continue
            
            doc = Document(environment,'to-be-implemented')
            
            analysis_folder = f['Key'].split('/')[0]+'/analysis/'
            doc.repair_widget_output = get_repair_widget_output(bucket,analysis_folder)
            doc.repair_widget_output_available = True

            if include_strategies:
                doc.dta_output, doc.dta_output_with_strategies = get_dta_output(bucket,f['Key'],include_strategies=True)
            else:
                doc.dta_output = get_dta_output(bucket,f['Key'],include_strategies)            
            
            doc.dta_output_available = True
            
            yield doc